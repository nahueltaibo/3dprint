# Folders

This folder must contain the model parts with descriptive name.

For example:

left_leg/left_leg_servo_cls6336hv.stl
left_leg/left_legservo_mg996r.stl

or 

rear_cover/rear_cover_16x2_rocket_switch

# Servo models

Please update the following list with links to the models online and if you need to add some clarification about the horns you used.

